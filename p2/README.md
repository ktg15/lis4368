> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

LIS4368

Kyle Gross

Project #2 Requirements:

Three Parts:

1. CRUD Capability
2. Server-side validation
3. Chapter Questions


>
>
>
>
>
#### Assignment Screenshot

*Screenshot of Valid User Form Entry:

![Screenshot of Valid User Form Entry](img/valid.png)

*Screenshot of Passed Validation:

![Screenshot of Passed Validation](img/passed.png)

*Screenshot of Display Data:

![Screenshot of Display Data](img/display.png)

*Screenshot of Modify Form:

![Screenshot of Modify Form](img/modify.png)

*Screenshot of Modified Data:

![Screenshot of Modified Data](img/modified.png)

*Screenshot of Delete Warning:

![Screenshot of Delete Warning](img/warning.png)

*Screenshot of Associated Database Changes:

![Screenshot of Associated Database Changes](img/changes.png)

![Screenshot of Associated Database Changes 2](img/changes_1.png)
