
## Kyle Gross


1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Screenshot of Query Result
    - Working Links

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Screenshot of ERD
    - Links to the following files:
         a. a3.mwb
         b. a3.sql

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Screenshot of Data Validation
    - Online Portfolio


5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Screenshot of Failed Validation
    - Screenshot of Passed Validation


6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Screenshots of Pre-, Post Valid User Form Entry
    - MYSQL Customer Table Entry


7. [P2 README.md](p2/README.md "My P2 README.md file")
    - CRUD capability
    - Server-side validation


