> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

LIS4368

Kyle Gross

Assignment #1 Requirements:

Three Parts:

1. Distributed Version Control with Git and Bitbucket
2. Development Installations    
3. Questions

README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running http://localhost:9999
* git commands with short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorials above 
(bitbucketstationlocations and myteamquotes).

> 
> 
> 
>
> #### Git commands w/short descriptions:

1. git init-Create a new local repository
2. git status-List the files you've changed and those you still need to add or commit
3. git add-Add one or more files to staging
4. git commit-Commit any new files you've added with 'git add'
5. git push-Send changes to the master branch of your remote repository
6. git pull-Fetch and merge changes on the remote server to your working directory
7. git diff-View all the merge conflicts

#### Assignment Screenshots:

*Screenshot of Tomcat running http://localhost9999:

![Screenshot of Tomcat](img/apache_tomcat.png)

*Screenshot of running java Hello*:

![Screenshot of Tomcat](img/Hello.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
