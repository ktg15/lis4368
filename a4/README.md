> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

LIS4368

Kyle Gross

Assignment #4 Requirements:

Three Parts:

1. Screenshots of Server-Side validation
2. Push files to Bitbucket
3. Chapter Questions


>
>
>
>
>
#### Assignment Screenshot

*Screenshot of Failed Validation:

![Screenshot of Failed Validation](img/fail.png)

*Screenshot of Passed Validation:

![Screenshot of Passed Validation](img/thanks.png)


