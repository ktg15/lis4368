> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

LIS4368

Kyle Gross

Assignment #3 Requirements:

Three Parts:

1. Screenshot of ERD
2. Links to the following files:
    a. a3.mwb
    b. a3.sql
3. Chapter Questions


>
>
>
>
>
#### Assignment Screenshot

*Screenshot of ERD:

![Screenshot of ERD](img/a3.png)

* Links to .mwb and .sql

[a3.mwb](a3.mwb)

[a3.sql](a3.sql)



