> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

LIS4368

Kyle Gross

Assignment #5 Requirements:

Three Parts:

1. Screenshots of Pre-, Post Valid User Form Entry
2. MYSQL Customer Table Entry
3. Chapter Questions


>
>
>
>
>
#### Assignment Screenshot

*Screenshot of Valid User Form Entry:

![Screenshot of Valid User Form Entry](img/valid.png)

*Screenshot of Passed Validation:

![Screenshot of Passed Validation](img/passed.png)

*Screenshot of Associated Database Entry:

![Screenshot of Associated Database Entry](img/db.png)